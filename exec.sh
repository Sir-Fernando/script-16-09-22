#!/bin/bash

echo "O comando utilizado para executar o script foi $0"
echo "O primeiro parâmetro é o: $1"
echo "O segundo parâmetro é o: $2"
echo "A quantidade de parâmetros utilizados foi $#"
echo
echo "Os parâmetros utilizados foram $*"
echo
echo '$0' "Apresenta o comando utilizado para executar o Script"
echo
echo '$1' "Apresenta o primeiro parâmetro passado ao chamar o comando $0"
echo
echo '$2' "Apresenta o segundo parâmetro passado ao chamdo o comando $0"
echo
echo '$#' "Apresenta a quantidade de parâmetros utilizados"
echo
echo '$*' "Apresenta quais foram os parâmetros utilizados"
