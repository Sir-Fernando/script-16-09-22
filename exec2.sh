#!/bin/bash

echo "Primeiro arquivo: $1"
echo "Segundo arquivo: $2"
echo "Terceiro arquivo: $3"

cat $1 > /tmp/tudojunto
cat $2 >> /tmp/tudojunto
cat $3 >> /tmp/tudojunto

echo "O nome dos arquivos foram armazenados em /tmp/tudojunto"
